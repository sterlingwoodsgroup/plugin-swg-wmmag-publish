<?php

function competition_init() {
	register_post_type( 'competition', array(
		'labels'            => array(
			'name'                => __( 'Competitions', 'swg-publish' ),
			'singular_name'       => __( 'Competition', 'swg-publish' ),
			'all_items'           => __( 'All Competitions', 'swg-publish' ),
			'new_item'            => __( 'New competition', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New competition', 'swg-publish' ),
			'edit_item'           => __( 'Edit competition', 'swg-publish' ),
			'view_item'           => __( 'View competition', 'swg-publish' ),
			'search_items'        => __( 'Search competitions', 'swg-publish' ),
			'not_found'           => __( 'No competitions found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No competitions found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent competition', 'swg-publish' ),
			'menu_name'           => __( 'Competitions', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail',  'page-attributes' ),
		'has_archive'       => false,
		'rewrite'           => array(  'slug' => 'competition', 'with_front' => false ),
//		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-tickets-alt',
		'show_in_rest'      => true,
		'rest_base'         => 'competition',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'competition_init' );

function competition_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['competition'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Competition updated. <a target="_blank" href="%s">View competition</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Competition updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Competition restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Competition published. <a href="%s">View competition</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Competition saved.', 'swg-publish'),
		8 => sprintf( __('Competition submitted. <a target="_blank" href="%s">Preview competition</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Competition scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview competition</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Competition draft updated. <a target="_blank" href="%s">Preview competition</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'competition_updated_messages' );
