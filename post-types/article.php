<?php

function article_init() {
	register_post_type( 'article', array(
		'labels'            => array(
			'name'                => __( 'Articles', 'swg-publish' ),
			'singular_name'       => __( 'Article', 'swg-publish' ),
			'all_items'           => __( 'All Articles', 'swg-publish' ),
			'new_item'            => __( 'New article', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New article', 'swg-publish' ),
			'edit_item'           => __( 'Edit article', 'swg-publish' ),
			'view_item'           => __( 'View article', 'swg-publish' ),
			'search_items'        => __( 'Search articles', 'swg-publish' ),
			'not_found'           => __( 'No articles found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No articles found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent article', 'swg-publish' ),
			'menu_name'           => __( 'Articles', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-media-document',
		'show_in_rest'      => true,
		'rest_base'         => 'article',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'article_init' );

function article_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['article'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Article updated. <a target="_blank" href="%s">View article</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Article updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Article restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Article published. <a href="%s">View article</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Article saved.', 'swg-publish'),
		8 => sprintf( __('Article submitted. <a target="_blank" href="%s">Preview article</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Article scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview article</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Article draft updated. <a target="_blank" href="%s">Preview article</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'article_updated_messages' );
