<?php

function technique_init() {
	register_post_type( 'technique', array(
		'labels'            => array(
			'name'                => __( 'Techniques', 'swg-publish' ),
			'singular_name'       => __( 'Technique', 'swg-publish' ),
			'all_items'           => __( 'All Techniques', 'swg-publish' ),
			'new_item'            => __( 'New technique', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New technique', 'swg-publish' ),
			'edit_item'           => __( 'Edit technique', 'swg-publish' ),
			'view_item'           => __( 'View technique', 'swg-publish' ),
			'search_items'        => __( 'Search techniques', 'swg-publish' ),
			'not_found'           => __( 'No techniques found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No techniques found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent technique', 'swg-publish' ),
			'menu_name'           => __( 'Techniques', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-media-document',
		'show_in_rest'      => true,
		'rest_base'         => 'technique',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'technique_init' );

function technique_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['technique'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Technique updated. <a target="_blank" href="%s">View technique</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Technique updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Technique restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Technique published. <a href="%s">View technique</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Technique saved.', 'swg-publish'),
		8 => sprintf( __('Technique submitted. <a target="_blank" href="%s">Preview technique</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Technique scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview technique</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Technique draft updated. <a target="_blank" href="%s">Preview technique</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'technique_updated_messages' );
