<?php

function location_init() {
	register_post_type( 'location', array(
		'labels'            => array(
			'name'                => __( 'Locations', 'swg-publish' ),
			'singular_name'       => __( 'Location', 'swg-publish' ),
			'all_items'           => __( 'All Locations', 'swg-publish' ),
			'new_item'            => __( 'New Location', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New Location', 'swg-publish' ),
			'edit_item'           => __( 'Edit Location', 'swg-publish' ),
			'view_item'           => __( 'View Location', 'swg-publish' ),
			'search_items'        => __( 'Search Locations', 'swg-publish' ),
			'not_found'           => __( 'No Locations found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No Locations found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent Location', 'swg-publish' ),
			'menu_name'           => __( 'Locations', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 26,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-clipboard',
		'show_in_rest'      => true,
		'rest_base'         => 'location',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'location_init' );

function location_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['location'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Location updated. <a target="_blank" href="%s">View Location</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Location updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Location restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Location published. <a href="%s">View Location</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Location saved.', 'swg-publish'),
		8 => sprintf( __('Location submitted. <a target="_blank" href="%s">Preview Location</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Location scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Location</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Location draft updated. <a target="_blank" href="%s">Preview Location</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'location_updated_messages' );
