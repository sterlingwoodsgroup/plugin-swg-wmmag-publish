<?php

function wine_wizard_init() {
	register_post_type( 'wine-wizard', array(
		'labels'            => array(
			'name'                => __( 'Wine Wizard', 'swg-publish' ),
			'singular_name'       => __( 'Wine Wizard', 'swg-publish' ),
			'all_items'           => __( 'All Wine Wizard', 'swg-publish' ),
			'new_item'            => __( 'New Wine Wizard', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New Wine Wizard', 'swg-publish' ),
			'edit_item'           => __( 'Edit Wine Wizard', 'swg-publish' ),
			'view_item'           => __( 'View Wine Wizard', 'swg-publish' ),
			'search_items'        => __( 'Search Wine Wizard', 'swg-publish' ),
			'not_found'           => __( 'No Wine Wizard found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No Wine Wizard found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent Wine Wizard', 'swg-publish' ),
			'menu_name'           => __( 'Wine Wizard', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'wine-wizard',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'wine_wizard_init' );

function wine_wizard_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['wine-wizard'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Wine Wizard updated. <a target="_blank" href="%s">View Wine Wizard</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Wine Wizard updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Wine Wizard restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Wine Wizard published. <a href="%s">View Wine Wizard</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Wine Wizard saved.', 'swg-publish'),
		8 => sprintf( __('Wine Wizard submitted. <a target="_blank" href="%s">Preview Wine Wizard</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Wine Wizard scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Wine Wizard</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Wine Wizard draft updated. <a target="_blank" href="%s">Preview Wine Wizard</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'wine_wizard_updated_messages' );
