<?php

function conference_init() {
	register_post_type( 'conference', array(
		'labels'            => array(
			'name'                => __( 'Conferences', 'swg-publish' ),
			'singular_name'       => __( 'Conference', 'swg-publish' ),
			'all_items'           => __( 'All Conferences', 'swg-publish' ),
			'new_item'            => __( 'New conference', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New conference', 'swg-publish' ),
			'edit_item'           => __( 'Edit conference', 'swg-publish' ),
			'view_item'           => __( 'View conference', 'swg-publish' ),
			'search_items'        => __( 'Search conferences', 'swg-publish' ),
			'not_found'           => __( 'No conferences found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No conferences found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent conference', 'swg-publish' ),
			'menu_name'           => __( 'Conferences', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail',  'page-attributes' ),
		'has_archive'       => false,
		'rewrite'           => array(  'slug' => 'conference', 'with_front' => false ),
//		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-tickets-alt',
		'show_in_rest'      => true,
		'rest_base'         => 'conference',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'conference_init' );

function conference_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['conference'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Conference updated. <a target="_blank" href="%s">View conference</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Conference updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Conference restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Conference published. <a href="%s">View conference</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Conference saved.', 'swg-publish'),
		8 => sprintf( __('Conference submitted. <a target="_blank" href="%s">Preview conference</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Conference scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview conference</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Conference draft updated. <a target="_blank" href="%s">Preview conference</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'conference_updated_messages' );
