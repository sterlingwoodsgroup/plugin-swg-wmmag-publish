<?php

function directory_init() {
	register_taxonomy( 'directory', array( 'location' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Directories', 'swg-publish' ),
			'singular_name'              => _x( 'Directory', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Directories', 'swg-publish' ),
			'popular_items'              => __( 'Popular Directories', 'swg-publish' ),
			'all_items'                  => __( 'All Directories', 'swg-publish' ),
			'parent_item'                => __( 'Parent Directory', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Directory:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Directory', 'swg-publish' ),
			'update_item'                => __( 'Update Directory', 'swg-publish' ),
			'add_new_item'               => __( 'New Directory', 'swg-publish' ),
			'new_item_name'              => __( 'New Directory', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Directories with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Directories', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Directories', 'swg-publish' ),
			'not_found'                  => __( 'No Directories found.', 'swg-publish' ),
			'menu_name'                  => __( 'Directories', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'directory',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'directory_init' );
