<?php

function varietal_init() {
	register_taxonomy( 'varietal', array( 'article', 'mr-wizard', 'wine-wizard', 'technique', 'resource',  ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Varietals', 'swg-publish' ),
			'singular_name'              => _x( 'Varietal', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Varietals', 'swg-publish' ),
			'popular_items'              => __( 'Popular Varietals', 'swg-publish' ),
			'all_items'                  => __( 'All Varietals', 'swg-publish' ),
			'parent_item'                => __( 'Parent Varietal', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Varietal:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Varietal', 'swg-publish' ),
			'update_item'                => __( 'Update Varietal', 'swg-publish' ),
			'add_new_item'               => __( 'New Varietal', 'swg-publish' ),
			'new_item_name'              => __( 'New Varietal', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Varietals with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Varietals', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Varietals', 'swg-publish' ),
			'not_found'                  => __( 'No Varietals found.', 'swg-publish' ),
			'menu_name'                  => __( 'Varietals', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'varietal',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'varietal_init' );
