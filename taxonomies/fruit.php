<?php

function fruit_init() {
	register_taxonomy( 'fruit', array( 'article', 'technique',  'wine-wizard' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Fruits', 'swg-publish' ),
			'singular_name'              => _x( 'Fruit', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Fruits', 'swg-publish' ),
			'popular_items'              => __( 'Popular Fruits', 'swg-publish' ),
			'all_items'                  => __( 'All Fruits', 'swg-publish' ),
			'parent_item'                => __( 'Parent Fruit', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Fruit:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Fruit', 'swg-publish' ),
			'update_item'                => __( 'Update Fruit', 'swg-publish' ),
			'add_new_item'               => __( 'New Fruit', 'swg-publish' ),
			'new_item_name'              => __( 'New Fruit', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Fruits with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Fruits', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Fruits', 'swg-publish' ),
			'not_found'                  => __( 'No Fruits found.', 'swg-publish' ),
			'menu_name'                  => __( 'Fruits', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'fruit',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'fruit_init' );
