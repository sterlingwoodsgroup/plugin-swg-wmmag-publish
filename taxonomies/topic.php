<?php

function topic_init() {
	register_taxonomy( 'topic', array( 'article', 'mr-wizard', 'wine-wizard', 'technique', 'resource', 'faq' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
//		'rewrite'           => true,
		'rewrite'           => array(  'slug' => 'topic', 'with_front' => false ),
		//'rewrite'           => array( 'hierarchical' => true, 'slug' => 'article/topic', 'with_front' => false),
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Topics', 'swg-publish' ),
			'singular_name'              => _x( 'Topic', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search topics', 'swg-publish' ),
			'popular_items'              => __( 'Popular topics', 'swg-publish' ),
			'all_items'                  => __( 'All topics', 'swg-publish' ),
			'parent_item'                => __( 'Parent topic', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent topic:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit topic', 'swg-publish' ),
			'update_item'                => __( 'Update topic', 'swg-publish' ),
			'add_new_item'               => __( 'New topic', 'swg-publish' ),
			'new_item_name'              => __( 'New topic', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate topics with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove topics', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used topics', 'swg-publish' ),
			'not_found'                  => __( 'No topics found.', 'swg-publish' ),
			'menu_name'                  => __( 'Topics', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'topic',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'topic_init' );
